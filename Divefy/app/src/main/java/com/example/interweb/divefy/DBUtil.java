package com.example.interweb.divefy;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOError;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.Buffer;
import java.nio.charset.MalformedInputException;
import org.bson.Document;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;


/**
 * Created by Interweb on 1/30/2016.
 */
public class DBUtil extends SQLiteOpenHelper {

    public DBUtil(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }


    private class CallAPI extends AsyncTask<Void,Void,ArrayList<Coord>>
    {

        @Override
        protected ArrayList<Coord> doInBackground(Void... params) {
            /*
            MongoClient mc = new MongoClient(new MongoClientURI("mongodb://admin:admin@ds049925.mongolab.com:49925/divetest"));

            MongoDatabase mdb = mc.getDatabase("divetest");


            FindIterable<Document> iterable = mdb.getCollection("Dive").find();
            iterable.forEach(new Block<Document>() {
                @Override
                public void apply(final Document document) {
                    System.out.println(document);
                }
            });

           */
            ArrayList<Coord> cds = new ArrayList<Coord>();

            try
            {

                URL url = new URL("http://divefy.com/dives/box/%5B%5B-76.309425,44.446951%5D,%5B-76.848314,44.215118%5D,%5B-76.579149,44.077158%5D,%5B-76.383476,44.106769%5D,%5B-76.309425,44.446951%5D%5D");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();

                conn.setRequestMethod("GET");
                conn.setRequestProperty("Accept", "application/xml");
                conn.connect();


                System.out.println(conn.getResponseCode());

                if (conn.getResponseCode() != 200)
                {
                    throw new RuntimeException("Failed L HTTP error code : " + conn.getResponseCode());
                }

                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                String apiOutput = br.readLine();
                while(apiOutput != null) {
                    System.out.println(apiOutput);
                    try {
                        JSONArray jarr = new JSONArray(apiOutput);
                        for (int i = 0; i < jarr.length(); i++) {
                            JSONObject jobj = jarr.getJSONObject(i);

                            double clat = jobj.getJSONObject("loc").getJSONArray("coordinates").getDouble(1);
                            double clong = jobj.getJSONObject("loc").getJSONArray("coordinates").getDouble(0);
                            String cname = jobj.getString("title");
                            String cdesc = jobj.getString("description");
                            int cdepth = jobj.getInt("depth");
                            Coord c = new Coord();
                            c.id = i;
                            c.name = cname;
                            c.depth = cdepth;
                            c.x = clat;
                            c.y = clong;
                            c.desc = cdesc;
                            cds.add(c);
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }



                    apiOutput = br.readLine();
                }

                conn.disconnect();


            }
            catch (MalformedURLException e)
            {
                e.printStackTrace();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }


            return cds;
        }


    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        //grabRemoteData(db);
        db.execSQL("create table dive (id integer primary key, name text, desc text, lat real, long real, depth integer)");

        ArrayList<Coord> cds = grabRemoteData();
        for (Coord c : cds) {
            ContentValues cv = new ContentValues();
            cv.put("name", c.name);
            cv.put("desc", c.desc);
            cv.put("lat", c.x);
            cv.put("long", c.y);
            cv.put("depth", c.depth);
            db.insert("dive", null, cv);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists dive"
        );
        onCreate(db);
    }

    public ArrayList<Coord> getAllCoord() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery ("select * from dive",null);
        c.moveToFirst();

        ArrayList<Coord> cds = new ArrayList<Coord>();
        while(c.isAfterLast() == false){
            Coord cd = new Coord();
            cd.x = c.getDouble(c.getColumnIndex("lat"));
            cd.y = c.getDouble(c.getColumnIndex("long"));
            cd.name = c.getString(c.getColumnIndex("name"));
            cd.id = c.getInt(c.getColumnIndex("id"));
            cd.desc = c.getString(c.getColumnIndex("desc"));
            cd.depth = c.getInt(c.getColumnIndex("depth"));
            cds.add(cd);
            c.moveToNext();
        }

        return cds;
    }

    public Coord getCoordById(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery ("select * from dive where id=" + id, null);
        c.moveToFirst();
        Coord cd = new Coord();
        while(c.isAfterLast() == false) {

            cd.x = c.getDouble(c.getColumnIndex("lat"));
            cd.y = c.getDouble(c.getColumnIndex("long"));
            cd.name = c.getString(c.getColumnIndex("name"));
            cd.id = c.getInt(c.getColumnIndex("id"));
            cd.desc = c.getString(c.getColumnIndex("desc"));
            cd.depth = c.getInt(c.getColumnIndex("depth"));
            c.moveToNext();
        }
        return cd;
    }


    public ArrayList<Coord> grabRemoteData()
    {
        ArrayList<Coord> cds;
        try {
            CallAPI callAPI = new CallAPI();
            AsyncTask<Void,Void,ArrayList<Coord>> at = callAPI.execute();
            cds = at.get();
        } catch (ExecutionException e) {
            cds = null;
            e.printStackTrace();
        } catch (InterruptedException e) {
            cds = null;
            e.printStackTrace();
        }

        return cds;
        /*
        /*
        RequestParams params = new RequestParams();
        params.put("id","data");

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int i, cz.msebera.android.httpclient.Header[] headers, byte[] bytes) {
                try {
                    JSONObject obj = new JSONObject(response);
                    if(obj.getBoolean("status")){

                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(int i, cz.msebera.android.httpclient.Header[] headers, byte[] bytes, Throwable throwable) {
                if(i == 400)
                {
                    System.p
                }
            }
        });
        */

    }
}
