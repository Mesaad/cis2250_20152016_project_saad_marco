package com.example.interweb.divefy;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MarkerActivity extends AppCompatActivity {

    DBUtil mydb = new DBUtil(this, null, null, 1);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marker);

        TextView tv = (TextView) findViewById(R.id.textViewMarker);
        Bundle b = getIntent().getExtras();
        int id = b.getInt("id");
        Coord c = mydb.getCoordById(id);
        tv.setText("" + c.name + "\n" + "\n" + "Depth: " + c.depth + "m\n" + "\n" + c.desc + "\n");
    }
}
