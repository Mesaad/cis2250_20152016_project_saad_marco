package com.example.interweb.divefy;

/**
 * Created by Interweb on 2/6/2016.
 */
public class Coord {
    public double x;
    public double y;
    public String name;
    public String desc;
    public int depth;
    public int id;
}
